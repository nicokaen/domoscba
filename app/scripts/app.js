'use strict';

/**
 * @ngdoc overview
 * @name domoscbaApp
 * @description
 * # domoscbaApp
 *
 * Main module of the application.
 */
angular
    .module('domoscbaApp', [
        'ngSanitize',
        'ngTouch',
        'ngAnimate',
        'ui.bootstrap',
        'angular-scroll-animate'
    ])
    .config(['$compileProvider', '$locationProvider', function ($compileProvider, $locationProvider) {
        $compileProvider.debugInfoEnabled(false);
        $locationProvider.html5Mode(true);
    }]);
