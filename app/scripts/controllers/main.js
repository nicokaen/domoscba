'use strict';

/**
 * @ngdoc function
 * @name domoscbaApp.controller:MainController
 * @description
 * # MainController
 * Controller of the domoscbaApp
 */
angular.module('domoscbaApp')
  .controller('MainController',
    function (DomosService, $http, $uibModal, $scope, $location, $timeout, $anchorScroll, $window, $rootScope) {

    var vm = this;

    vm.currentYear = new Date().getFullYear();
    vm.showMoreWorkshops = false;
    vm.viewVideo = false;
    vm.workshopsLimit = 4;

    vm.stats = DomosService.getStats();
    vm.about = DomosService.getAbout();
    vm.aboutBioArq = DomosService.getAboutBioArq();

    // Verify contact form response
    if ($location.hash() === 'contacto-enviado') {
      vm.contactFormSuccess = 'Mensaje enviado correctamente. Responderemos a la brevedad.';
    }

    // ScrollTo hash on page load
    $timeout(function () {
      $anchorScroll($location.hash());
    });

    // Default animations
    vm.animateElementIn = function ($el) {
      $el.removeClass('invisible');
      $el.addClass('animated fadeIn'); // this example leverages animate.css classes
    };

    vm.animateElementOut = function ($el) {
      $el.addClass('invisible');
      $el.removeClass('animated fadeIn'); // this example leverages animate.css classes
    };

    // Carousel next
    vm.next = function (carousel) {
      if (carousel.active === carousel.slides.length - 1) {
        carousel.active = 0;
      } else {
        carousel.active++;
      }
    };

    // Carousel prev
    vm.prev = function (carousel) {
      if (carousel.active === 0) {
        carousel.active = carousel.slides.length - 1;
      } else {
        carousel.active--;
      }
    };

    // Home Carousel Config
    vm.homeSlideshow = {};
    vm.homeSlideshow.slides = DomosService.getHomeSlides();
    angular.forEach(vm.homeSlideshow.slides, function (slide, index) {
      slide.id = index;
    });
    vm.homeSlideshow.myInterval = 5000;
    vm.homeSlideshow.noWrapSlides = false;
    vm.homeSlideshow.active = 0;
    // --

    // Madera Carousel Config
    vm.maderaSlideshow = {};
    vm.maderaSlideshow.slides = DomosService.getMaderaSlides();
    angular.forEach(vm.maderaSlideshow.slides, function (slide, index) {
      slide.id = index;
    });
    vm.maderaSlideshow.myInterval = 5000;
    vm.maderaSlideshow.noWrapSlides = false;
    vm.maderaSlideshow.active = 0;
    // --

    // Adicionales Carousel Config
    vm.metalSlideshow = {};
    vm.metalSlideshow.slides = DomosService.getAdicionalesSlides();
    angular.forEach(vm.metalSlideshow.slides, function (slide, index) {
      slide.id = index;
    });
    vm.metalSlideshow.myInterval = 5000;
    vm.metalSlideshow.noWrapSlides = false;
    vm.metalSlideshow.active = 0;
    // --

    // BioArquitectura Carousel Config
    vm.bioArqSlideshow = {};
    vm.bioArqSlideshow.slides = DomosService.getBioArqSlides();
    angular.forEach(vm.bioArqSlideshow.slides, function (slide, index) {
      slide.id = index;
    });
    vm.bioArqSlideshow.myInterval = 5000;
    vm.bioArqSlideshow.noWrapSlides = false;
    vm.bioArqSlideshow.active = 0;
    // --

    // Events Carousel Config
    vm.eventsSlideshow = {};
    vm.eventsSlideshow.slides = DomosService.getEventsSlides();
    angular.forEach(vm.eventsSlideshow.slides, function (slide, index) {
      slide.id = index;
    });
    vm.eventsSlideshow.myInterval = 5000;
    vm.eventsSlideshow.noWrapSlides = false;
    vm.eventsSlideshow.active = 0;
    // --

    vm.talleres = DomosService.getTalleres();
    vm.toggleShowMoreLimit = function () {
      vm.workshopsLimit = vm.workshopsLimit ? false : 4;
      vm.showMoreWorkshops = !vm.showMoreWorkshops;
    };

    vm.openGallery = function (currentImage, gallery, size) {

      if (!size) {
        size = 'md';
      }

      var modalInstance = $uibModal.open({
        templateUrl: 'views/gallery-modal.html',
        controller: 'GalleryModalController',
        controllerAs: 'GalleryModalCtrl',
        size: size,
        resolve: {
          currentImg: function () {
            return currentImage;
          },
          gallery: function () {
            return gallery;
          }
        }
      });
    };

    // Google analytics
    // $scope.$on('$viewContentLoaded', function(event) {
    //     $window.ga('send', 'pageview', { page: $location.url() });
    // });

    // $rootScope.$on('$routeChangeSuccess', function(){
    //     $window.ga('send', 'pageview', $location.path());
    // });
  });
