'use strict';

/**
 * @ngdoc function
 * @name domoscbaApp.controller:GalleryModalController
 * @description
 * # GalleryModalController
 * Controller of the domoscbaApp
 */
angular.module('domoscbaApp')
  .controller('GalleryModalController', function ($uibModalInstance, currentImg, gallery, DomosService) {

    var vm = this;
    vm.currentImg = currentImg;
    vm.slideshow = {};
    vm.slideshow.hideControls = true;

    // Carousel Config
    if (gallery === 'bioArq') {
      vm.slideshow.hideControls = false;
      vm.slideshow.slides = DomosService.getBioArqSlides();
    } else if (gallery === 'events') {
      vm.slideshow.hideControls = false;
      vm.slideshow.slides = DomosService.getEventsSlides();
    } else if (gallery === 'madera') {
      vm.slideshow.slides = DomosService.getMaderaSlides();
    } else if (gallery === 'adicionales') {
      vm.slideshow.slides = DomosService.getAdicionalesSlides();
    } else if (gallery === 'taller') {
      vm.slideshow.slides = DomosService.getTalleres();
    }
    angular.forEach(vm.slideshow.slides, function (slide, index) {
      slide.id = index;
    });
    vm.slideshow.myInterval = 0; // 5000
    vm.slideshow.noWrapSlides = false;

    vm.slideshow.active = 0;
    if (vm.currentImg) {
      var currentImgIndex = vm.slideshow.slides.findIndex(function (slides) {
        return slides.image === vm.currentImg;
      });

      if (currentImgIndex >= 0) {
        vm.slideshow.active = currentImgIndex;
      }
    }
    // --

    vm.ok = function () {
      $uibModalInstance.close();
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
