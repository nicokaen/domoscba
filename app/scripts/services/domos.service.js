'use strict';

angular.module('domoscbaApp')
  .factory('DomosService', ['$window', function (win) {

    const service = {};

    // HOME STATS
    service.getStats = function () {
      return [
        {
          label: 'Domos',
          icon: 'fa-star',
          total: '+122'
        },
        {
          label: 'Alumnos',
          icon: 'fa-user-o',
          total: '+455'
        },
        {
          label: 'Me gusta',
          icon: 'fa-facebook',
          total: '+14K',
          link: 'https://www.facebook.com/domoscordoba'
        },
        {
          label: 'Seguidores',
          icon: 'fa-instagram',
          total: '+12.5K',
          link: 'https://www.instagram.com/domoscordoba/'
        }
      ];
    };

    // ABOUT DATA
    service.getAbout = function () {
      return [
        {
          name: 'Juanjo Sánchez',
          role: 'Socio Fundador en Domos Córdoba',
          interest: 'Emprendedor, capacitador y activista del desarrollo sustentable.',
          quote: {
            message: '“No intentes cambiar un sistema, construye uno nuevo que haga que el anterior se vuelva obsoleto”',
            author: 'Bucky Fuller'
          },
          pic: 'images/juanjo-profile.jpg'
        },
        {
          name: 'Juan Ignacio Olveira',
          role: 'Socio Fundador en Domos Córdoba',
          interest: 'Emprendedor, comprometido con el crecimiento interior y cuidado del medio ambiente.',
          quote: {
            message: '“No es que cuando se disuelve el límite voy a dar el Paso, Dar el Paso es lo que disuelve el límite”',
            author: 'JL Parise'
          },
          pic: 'images/juani-profile.jpg'
        }
      ];
    };

    // ABOUT BIO-ARQ
    service.getAboutBioArq = function () {
      return {
        name: 'Arq. Armando Gross',
        role: 'Fundador del estudio Van-Gross con 12 años de trayectoria profesional en el diseño bioclimático y construcción natural.',
        interest: 'Docente extensionista UNC. Creador del TABI. Activista en el camino de la Permacultura.',
        pic: 'images/profile-arq.jpg'
      };
    };

    // HOME SLIDESHOW
    service.getHomeSlides = function () {
      return [
        {
          image: 'images/portada/portada-construcciones-semiesfericas.jpg',
          subtext: 'CONSTRUCCIONES SEMIESFÉRICAS'
        },
        {
          image: 'images/portada/portada-kits-madera.jpg',
          subtext: 'KITS MADERA'
        },
        {
          image: 'images/portada/portada-bioarquitectura.jpg',
          subtext: 'BIOARQUITECTURA'
        },
        {
          image: 'images/portada/portada-alquileres-eventos.jpg',
          subtext: 'ALQUILERES Y EVENTOS'
        },
        {
          image: 'images/portada/portada-talleres.jpg',
          subtext: 'TALLERES Y CAPACITACIONES'
        },
        {
          image: 'images/portada/portada-corcho.jpg',
          subtext: 'CORCHO NATURAL PROYECTADO',
          description: 'Una revolución en materia de revestimientos constructivos'
        },
        {
          image: 'images/portada/portada-internacional.jpg',
          subtext: 'INTERNACIONAL',
          link: 'https://store.pacificdomes.com/?wpam_id=48'
        },
      ];
    };

    service.getMaderaSlides = function () {
      return [
        {
          image: 'images/madera/domos-madera-A.jpg',
          name: 'Kit Domo F2 ~ ø3 mts ~ ¾ de esfera',
          description: 'Ideal para dormi, refugios, e invernaderos.',
          details: [
            { label: 'Superficie', text: '7 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x3"' },
            { label: 'Cobertura', text: 'OSB estructural 9.5 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-B.jpg',
          name: 'Kit Domo F2 ~ ø4 mts ~ ¾ de esfera',
          description: 'Ideal para dormi, home estudio, refugios, e invernaderos',
          details: [
            { label: 'Superficie', text: '13 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x3"' },
            { label: 'Cobertura', text: 'OSB estructural 9.5 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-C.jpg',
          name: 'Kit Domo F3 ~ ø5 mts ~ ⅝ de esfera',
          description: 'Ideal para glamping, home estudio e invernaderos.',
          details: [
            { label: 'Superficie', text: '20 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x3"' },
            { label: 'Cobertura', text: 'OSB estructural 9.5 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-D.jpg',
          name: 'Kit Domo F3 ~ ø6 mts ~ ⅝ de esfera',
          description: 'Ideal para glamping, home estudio y loft.',
          details: [
            { label: 'Superficie', text: '30 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x3"' },
            { label: 'Cobertura', text: 'OSB estructural 9.5 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-E.jpg',
          name: 'Kit Domo F4 ~ ø7 mts ~ ⅝ de esfera',
          description: 'Ideal para cabañas, loft, tiny house con entre piso.',
          details: [
            { label: 'Superficie', text: '40 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x4"' },
            { label: 'Cobertura', text: 'OSB estructural 11,1 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-F.jpg',
          name: 'Kit Domo F4 ~ ø8 mts ~ ⅝ de esfera',
          description: 'Ideal para cabañas y viviendas 2 dormitorios con entrepiso.',
          details: [
            { label: 'Superficie', text: '52 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x4"' },
            { label: 'Cobertura', text: 'OSB estructural 11,1 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-G.jpg',
          name: 'Kit Domo F5 ~ ø9 mts ~ ⅝ de esfera',
          description: 'Ideal para SUM y viviendas 2 dormitorios con entrepiso.',
          details: [
            { label: 'Superficie', text: '66 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x4"' },
            { label: 'Cobertura', text: 'OSB estructural 11,1 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-H.jpg',
          name: 'Kit Domo F5 ~ ø10 mts ~ ⅝ de esfera',
          description: 'Ideal para SUM y viviendas 2 dormitorios con entrepiso.',
          details: [
            { label: 'Superficie', text: '81 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x4"' },
            { label: 'Cobertura', text: 'OSB estructural 11,1 mm ' },
          ]
        },
        {
          image: 'images/madera/domos-madera-I.jpg',
          name: 'Kit Domo F6 ~ ø11 mts ~ ½ esfera',
          description: 'Ideal para SUM y viviendas 3 dormitorios con entrepiso.',
          details: [
            { label: 'Superficie', text: '98 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x4"' },
            { label: 'Cobertura', text: 'OSB estructural 11,1 mm' },
          ]
        },
        {
          image: 'images/madera/domos-madera-J.jpg',
          name: 'Kit Domo F6 ~ ø13 mts ~ ½ esfera',
          description: 'Ideal para SUM y viviendas 3 dormitorios con entrepiso.',
          details: [
            { label: 'Superficie', text: '135 m2' },
            { label: 'Estructura', text: 'Madera de pino 2x4"' },
            { label: 'Cobertura', text: 'OSB estructural 11,1 mm' },
          ]
        },
      ];
    };

    service.getAdicionalesSlides = function () {
      return [
        {
          image: 'images/adicionales/adicionales-corcho-natural-proyectado.jpeg',
          name: 'Corcho Natural Proyectado',
          description: [
            'El mejor revestimiento exterior para tu domo.',
            'Impermeable, transpirable, elástico, aislante térmico y acústico.',
            'Servicio de Proyección en prov. de Córdoba.',
          ]
        },
        {
          image: 'images/adicionales/adicionales-masilla-termica.jpeg',
          name: 'Masilla Térmica para el tomado de juntas.',
          description: [
            'Masilla Térmica para el tomado de juntas.',
            'Masilla de extraordinaria calidad con gránulos de corcho natural y fibras de vidrio',
            'Dota a las superficies en que se aplica de corrección térmica y acústica.',
          ]
        },
        {
          image: 'images/adicionales/adicionales-suberpaint.jpeg',
          name: 'Suberpaint',
          description: [
            'Pintura de fachadas y cubiertas, especial anti-fisuras, fabricada con polímeros acrílicos puros en emulsión de excelente calidad.',
            'Con muy baja conductividad térmica.',
          ]
        },
        {
          image: 'images/adicionales/adicionales-aberturas-madera.jpeg',
          name: 'Aberturas de Madera',
          description: [
            'Ventana proyectante confeccionada a medida.',
            'Madera de eucaliptus grandis con impregnante.',
            'Incluye pistón y herrajes.',
            'No incluye vidrio.',
          ]
        },
        {
          image: 'images/adicionales/adicionales-aberturas-aluminio.jpeg',
          name: 'Aberturas de Aluminio',
          description: [
            'Ventanas proyectantes confeccionada a medida.',
            'Incluye premarco de madera 1x4 en pino.',
            'Incluye vidrio Laminado 3+3.',
          ]
        },
        {
          image: 'images/adicionales/adicionales-marcos-madera.jpeg',
          name: 'Marcos de Madera',
          description: [
            'Confeccionado a medida para vidrio fijo.',
            'Eucaliptus grandis 2x4" con impregnante.',
            'Fresado según vidrio.',
          ]
        },
        {
          image: 'images/adicionales/adicionales-osb-interior.jpeg',
          name: 'OSB para Interior',
          description: [
            'Añadí también los triángulos en osb estructural para el recubrimiento interior.'
          ]
        },
        {
          image: 'images/adicionales/adicionales-puertas-madera.jpeg',
          name: 'Puertas de Madera',
          description: [
            'Eucaliptus Grandis más entablonado de madera.',
            'Tamaños estándar y personalizados.',
          ]
        },
        {
          image: 'images/adicionales/adicionales-portico-acceso.jpeg',
          name: 'Pórtico de Acceso',
          description: [
            'Diseño y confección de bastidores laterales y alero de ingreso.',
            'Construido en madera de pino con impregnante y osb estructural.',
            'No incluye revestimiento final.',
          ]
        },
      ];
    };

    service.getBioArqSlides = function () {
      return [
        { image: 'images/bio-arq/1.jpeg' },
        { image: 'images/bio-arq/2.jpeg' },
        { image: 'images/bio-arq/3.jpeg' },
        { image: 'images/bio-arq/4.jpeg' },
        { image: 'images/bio-arq/5.jpeg' },
        { image: 'images/bio-arq/6.jpeg' },
        { image: 'images/bio-arq/7.jpeg' },
        { image: 'images/bio-arq/8.jpeg' },
        { image: 'images/bio-arq/9.jpeg' },
        { image: 'images/bio-arq/10.jpeg' },
        { image: 'images/bio-arq/11.jpeg' },
        { image: 'images/bio-arq/12.jpeg' },
        { image: 'images/bio-arq/13.jpeg' },
        { image: 'images/bio-arq/14.jpeg' },
        { image: 'images/bio-arq/15.jpeg' },
        { image: 'images/bio-arq/16.jpeg' },
        { image: 'images/bio-arq/17.jpeg' },
        { image: 'images/bio-arq/18.jpeg' },
        { image: 'images/bio-arq/19.jpeg' },
        { image: 'images/bio-arq/20.jpeg' },
        { image: 'images/bio-arq/21.jpeg' },
        { image: 'images/bio-arq/22.jpeg' },
        { image: 'images/bio-arq/23.jpeg' },
      ];
    };

    service.getEventsSlides = function () {
      return [
        {
          image: 'images/eventos/eventos-1.jpg',
          size: 'lg'
        },
        {
          image: 'images/eventos/eventos-2.jpg',
        },
        {
          image: 'images/eventos/eventos-3.jpg',
        },
        {
          image: 'images/eventos/eventos-4.jpg',
        },
        {
          image: 'images/eventos/eventos-5.jpg',
        },
        {
          image: 'images/eventos/eventos-6.jpg',
          size: 'lg'
        },
        {
          image: 'images/eventos/eventos-7.jpg',
          size: 'lg'
        },
        {
          image: 'images/eventos/eventos-lolla.jpg'
        },
        {
          image: 'images/eventos/domos-cordoba-05.jpg'
        }
      ];
    };

    service.getTalleres = function () {
      return [
        {
          subtitle: 'Taller Intensivo Domos Geodésicos',
          image: 'images/taller/taller-18.jpg',
          title: 'Taller 18',
          detail: '2 y 3 de noviembre 2019, Los Aromos, Córdoba  Argentina',
        },
        {
          subtitle: 'Taller Intensivo Domos Geodésicos',
          image: 'images/taller/taller-17.jpg',
          title: 'Taller 17',
          detail: '17 y 18 de agosto 2019, Los Aromos, Córdoba  Argentina',
        },
        {
          subtitle: 'Taller Intensivo Domos Geodésicos',
          image: 'images/taller/taller-16.jpg',
          title: 'Taller 16',
          detail: '14 de abril 2019, Jardín Botánico Puerto Morelos, México',
        },
        {
          subtitle: 'Taller Intensivo Domos Geodésicos',
          image: 'images/taller/taller-15.jpg',
          title: 'Taller 15',
          detail: '23 y 24 de marzo 2019, Los Aromos, Córdoba  Argentina',
        },
        {
          subtitle: 'Taller Introductorio Domos de Madera',
          image: 'images/taller/taller-14.jpg',
          title: 'Taller 14',
          detail: '20 de enero 2019, Pinamar, Argentina',
        },
        {
          subtitle: 'Taller Bioarquitectura Geodésica',
          image: 'images/taller/taller-13.jpg',
          title: 'Taller 13',
          detail: '8 y 9 de diciembre 2018, Valle de Paravachasca, Córdoba Argentina',
        },
        {
          subtitle: 'Taller Intensivo de Domos Geodésicos',
          image: 'images/taller/taller-12.jpg',
          title: 'Taller 12',
          detail: '7 de octubre, San Isidro, Buenos Aires',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=2018929768170806'
        },
        {
          subtitle: 'Taller Intensivo de Domos Geodésicos',
          image: 'images/taller/taller-11.jpg',
          title: 'Taller 11',
          detail: '8 y 9 de septiembre, Neuquén, Argentina.',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=2062054553858327'
        },
        {
          subtitle: 'Taller Intensivo de Domos Geodésicos',
          image: 'images/taller/taller-10.jpg',
          title: 'Taller 10',
          detail: '2 y 3 de Junio, Barcelona, Europa.',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1940333916030392'
        },
        {
          subtitle: 'Taller Intensivo de Domos Geodésicos',
          image: 'images/taller/taller-9.jpg',
          title: 'Taller 9',
          detail: '26 y 27 de mayo, Comodoro Rivadavia, Chubut, Argentina.',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1811898205540631'
        },
        {
          subtitle: 'Taller Intensivo de Domos Geodésicos',
          image: 'images/taller/taller-8.jpg',
          title: 'Taller 8',
          detail: '24 y 25 de Febrero 2018 en Fundación Umepay, Córdoba, Argentina',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=2061718903891892'
        },
        {
          subtitle: 'Taller Intensivo de Domos Geodésicos',
          image: 'images/taller/taller-7.jpg',
          title: 'Taller 7',
          detail: '2 y 3 Diciembre 2017 <br> Shambala Hostel, Villa los aromos, Córdoba.',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1628925737171213'
        },
        {
          subtitle: 'Aprendé a construir Domos Geodésicos',
          image: 'images/taller/taller-6.jpg',
          title: 'Taller 6',
          detail: '26 y 27 de agosto 2017 <br> Shambala Hostel, Villa los aromos, Córdoba.',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1538900452840409'
        },
        {
          subtitle: 'Workshop, La Arquitectura en la Permacultura',
          image: 'images/taller/taller-5.jpg',
          title: 'Taller 5',
          detail: '9 de Julio 2017 <br> FAUDI, UNC.',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1487303808000074'
        },
        {
          subtitle: 'Aprendé a construir Domos Geodésicos',
          image: 'images/taller/taller-4.jpg',
          title: 'Taller 4',
          detail: '8 - 9 / Abril - 2017 <br> Tanti - Córdoba',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1397975396932916'
        },
        {
          subtitle: 'Aprendé a construir Domos Geodésicos',
          image: 'images/taller/taller-3.jpg',
          title: 'Taller 3',
          detail: 'Febrero 2017 <br> Punta del Diablo - Uruguay',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1342522299144893'
        },
        {
          subtitle: 'Aprendé a construir Domos Geodésicos',
          image: 'images/taller/taller-2.jpg',
          title: 'Taller 2',
          detail: 'Noviembre 2016 <br> Tanti - Córdoba',
          album: 'https://www.facebook.com/251885181541949/photos/?tab=album&album_id=1233407270056397'
        },
        {
          subtitle: 'Aprendé a construir Domos Geodésicos',
          image: 'images/taller/taller-1.jpg',
          title: 'Taller 1',
          detail: 'Agosto 2016 <br> Tanti - Córdoba',
          album: 'https://www.facebook.com/pg/domoscordoba/photos/?tab=album&album_id=1304682886262168'
        }
      ];
    };

    return service;

  }]);
